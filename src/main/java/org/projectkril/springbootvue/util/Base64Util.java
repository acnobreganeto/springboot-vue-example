package org.projectkril.springbootvue.util;

import java.util.Base64;

public class Base64Util {
    public static String encodeToBase64(String input) {
        return Base64.getEncoder().encodeToString(input.getBytes());
    }

    public static String decodeFromBase64(String input) {
        if (input != null && !input.isBlank()) {
            try {
                return new String(Base64.getDecoder().decode(input));
            } catch (IllegalArgumentException e) {
                return null;
            }
        } else {
            return null;
        }
    }
}