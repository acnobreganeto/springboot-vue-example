package org.projectkril.springbootvue.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class Base64UtilTest {
    @Test
    public void encodeToBase64() {
        String input = Base64Util.encodeToBase64("user@gmail.com:UserPassword");

        System.out.println(input);

        assertNotNull(input);
    }
}
